package com.example.quanlynhahang;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.quanlynhahang.adapter.BanAdapter;
import com.example.quanlynhahang.dao.BanDAO;
import com.example.quanlynhahang.model.Ban;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class QuanLyBanActivity extends AppCompatActivity {
    RecyclerView rv_dsb;
    Context c;
    FloatingActionButton fl_them;

    List<Ban> list;
    BanDAO banDAO;
    BanAdapter banAdapter;
    Ban ban;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quan_ly_ban);

        rv_dsb=findViewById(R.id.rv_dsb);
        rv_dsb.setHasFixedSize(true);
        rv_dsb.setLayoutManager(new LinearLayoutManager(c));
        showDSB();

        fl_them=findViewById(R.id.fl_them);
        themBan();
    }

    public void showDSB(){
        banDAO=new BanDAO(QuanLyBanActivity.this);
        list=new ArrayList<Ban>();
        list=banDAO.getAll();
        banAdapter=new BanAdapter(QuanLyBanActivity.this,list);
        rv_dsb.setAdapter(banAdapter);
    }

    public void capNhatLV(){
        banAdapter.notifyItemInserted(list.size());
        banAdapter.notifyDataSetChanged();
    }

    public void themBan(){
        fl_them.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(QuanLyBanActivity.this);
                dialog.setContentView(R.layout.them_ban_layout);

                final Spinner spChonViTri=dialog.findViewById(R.id.spChonViTri);
                final TextView tvViTri=dialog.findViewById(R.id.tvViTri);
                final EditText edtMaBan=dialog.findViewById(R.id.edtMaBan);
                final Button btnThem=dialog.findViewById(R.id.btnThem);


                //spinner chọn vị trí
                List<String> list=new ArrayList<>();
                list.add("Tầng 1");
                list.add("Tầng 2");
                list.add("Tầng 3");
                ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(
                        QuanLyBanActivity.this,android.R.layout.simple_spinner_item,list);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spChonViTri.setAdapter(arrayAdapter);
                spChonViTri.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        tvViTri.setText(spChonViTri.getSelectedItem().toString());
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                btnThem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (TextUtils.isEmpty(tvViTri.getText().toString())) {
                            Toast.makeText(getApplicationContext(), "mời chọn vị trí bàn...", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (TextUtils.isEmpty(edtMaBan.getText().toString())) {
                            Toast.makeText(getApplicationContext(), "mời nhập mã bàn...", Toast.LENGTH_LONG).show();
                            return;
                        }

                        ban=new Ban();
                        banDAO=new BanDAO(QuanLyBanActivity.this);
                        ban.setViTri(tvViTri.getText().toString());
                        ban.setmaBan(edtMaBan.getText().toString());
                        ban.setTrangThai("trong");
                        banDAO.insert(ban);
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }
}
