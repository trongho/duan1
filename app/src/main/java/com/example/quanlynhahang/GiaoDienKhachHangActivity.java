package com.example.quanlynhahang;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class GiaoDienKhachHangActivity extends AppCompatActivity {
    Button btnThongTinKhachHang,btnGopY,btnDatBan,btnXemDuongDi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_giao_dien_khach_hang);

        btnThongTinKhachHang=findViewById(R.id.btnThongTinKhachHang);
        btnGopY=findViewById(R.id.btnGopY);
        btnDatBan=findViewById(R.id.btnDatBan);
        btnXemDuongDi=findViewById(R.id.btnXemDuongDi);

        hienThongTinKhachHang();
        khachHangGopY();
        datBan();
        xemDuongDi();
    }

    public void hienThongTinKhachHang(){
        btnThongTinKhachHang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GiaoDienKhachHangActivity.this,ThongTinKhachHangActivity.class);
                startActivity(intent);
            }
        });
    }

    public void khachHangGopY(){
        btnGopY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GiaoDienKhachHangActivity.this,GopYKhachHangActivity.class);
                startActivity(intent);
            }
        });
    }

    public void datBan(){
        btnDatBan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GiaoDienKhachHangActivity.this,DatBanActivity.class);
                startActivity(intent);
            }
        });
    }
    public void xemDuongDi(){
        btnXemDuongDi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(GiaoDienKhachHangActivity.this,XemDuongDiActivity.class);
                startActivity(intent);
            }
        });
    }
}
