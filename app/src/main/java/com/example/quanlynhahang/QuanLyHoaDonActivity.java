package com.example.quanlynhahang;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;

import com.example.quanlynhahang.adapter.HoaDonBanAdapter;
import com.example.quanlynhahang.dao.HoaDonBanDAO;
import com.example.quanlynhahang.model.HoaDonBan;

import java.util.ArrayList;
import java.util.List;

public class QuanLyHoaDonActivity extends AppCompatActivity {
    RecyclerView rv_dshd;
    Context c;

    HoaDonBan hoaDonBan;
    List<HoaDonBan> list;
    HoaDonBanDAO hoaDonBanDAO;
    HoaDonBanAdapter hoaDonBanAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quan_ly_hoa_don);

        rv_dshd=findViewById(R.id.rv_dshd);
        rv_dshd.setHasFixedSize(true);
        rv_dshd.setLayoutManager(new LinearLayoutManager(c));

        showDSHD();
    }

    public void showDSHD(){
        list=new ArrayList<>();
        hoaDonBanDAO=new HoaDonBanDAO(QuanLyHoaDonActivity.this);
        list=hoaDonBanDAO.getAll();
        hoaDonBanAdapter=new HoaDonBanAdapter(QuanLyHoaDonActivity.this,list);
        rv_dshd.setAdapter(hoaDonBanAdapter);
    }

    public void capNhatLV(){
        hoaDonBanAdapter.notifyItemInserted(list.size());
        hoaDonBanAdapter.notifyDataSetChanged();
    }
}
