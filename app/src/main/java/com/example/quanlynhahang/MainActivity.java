package com.example.quanlynhahang;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button btnKhachHang,btnQuanLy,btnNhanVien;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnKhachHang=findViewById(R.id.btn_khachhang);
        btnQuanLy=findViewById(R.id.btn_quanly);
        btnNhanVien=findViewById(R.id.btn_nhanvien);

        btnKhachHang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,DangNhapKhachHangActivity.class);
                startActivity(intent);
            }
        });

        btnQuanLy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, DangNhapQuanLyActivity.class);
                startActivity(intent);
            }
        });

        btnNhanVien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, DangNhapNhanVienActivity.class);
                startActivity(intent);
            }
        });
    }
}
