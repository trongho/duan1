package com.example.quanlynhahang;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.Toast;

import com.example.quanlynhahang.adapter.MonAnAdapter2;
import com.example.quanlynhahang.dao.MonAnDao;
import com.example.quanlynhahang.model.MonAn;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class KhaiViFragment extends Fragment {
    GridView gvMonAn;

    MonAn monAn;
    List<MonAn> list;
    MonAnDao monAnDao;
    MonAnAdapter2 monAnAdapter2;

    public KhaiViFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_khai_vi, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        gvMonAn=view.findViewById(R.id.gvMonAn);


        monAnDao=new MonAnDao(getContext(),this);
        list=new ArrayList<MonAn>();
        list=monAnDao.getKhaiVi();
        monAnAdapter2=new MonAnAdapter2(list,getContext());
        gvMonAn.setAdapter(monAnAdapter2);
    }

    public void capNhatLV(){
        monAnAdapter2.notifyDataSetChanged();
    }

}
