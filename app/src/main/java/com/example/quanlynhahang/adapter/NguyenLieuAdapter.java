package com.example.quanlynhahang.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quanlynhahang.R;
import com.example.quanlynhahang.model.NguyenLieu;

import java.util.List;

public class NguyenLieuAdapter extends RecyclerView.Adapter<NguyenLieuAdapter.ViewHolder> {
    Context context;
    List<NguyenLieu> list;
    NguyenLieu nguyenLieu;

    public NguyenLieuAdapter(Context context,List<NguyenLieu> list) {
        this.context=context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvTenNguyenLieu,tvSoLuong;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTenNguyenLieu=itemView.findViewById(R.id.tvTenNguyenLieu);
            tvSoLuong=itemView.findViewById(R.id.tvSoLuong);
        }

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Tao view va gan layout vao view
        View v = LayoutInflater.from(context)
                .inflate(R.layout.one_cell_nguyen_lieu, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        nguyenLieu=list.get(i);

        viewHolder.tvTenNguyenLieu.setText(nguyenLieu.getTenNguyenLieu());
        viewHolder.tvSoLuong.setText("0");

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

