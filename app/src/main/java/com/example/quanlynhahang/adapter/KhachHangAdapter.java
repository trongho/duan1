package com.example.quanlynhahang.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quanlynhahang.R;
import com.example.quanlynhahang.ThongTinKhachHangActivity;
import com.example.quanlynhahang.dao.KhachHangDAO;
import com.example.quanlynhahang.model.KhachHang;
import com.squareup.picasso.Picasso;

import java.util.List;

public class KhachHangAdapter extends RecyclerView.Adapter<KhachHangAdapter.ViewHolder> {
    static Context context;
    List<KhachHang> list;
    KhachHang khachHang;

    public KhachHangAdapter(Context context,List<KhachHang> list) {
        this.context=context;
        this.list = list;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public RelativeLayout rl_layout;
        public TextView tvTenKH,tvEmail,tvSDT,tvDiaChi;
        public ImageView ivAnhDaiDien;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            rl_layout=itemView.findViewById(R.id.rl_layout);
            ivAnhDaiDien=itemView.findViewById(R.id.imgAnhDaiDien);
            tvTenKH=itemView.findViewById(R.id.tvTenKH);
            tvEmail=itemView.findViewById(R.id.tvEmail);
            tvSDT=itemView.findViewById(R.id.tvSDT);
            tvDiaChi=itemView.findViewById(R.id.tvDiaChi);
        }

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Tao view va gan layout vao view
        View v = LayoutInflater.from(context)
                .inflate(R.layout.one_cell_khach_hang, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        khachHang=list.get(i);

        viewHolder.tvTenKH.setText(khachHang.getTenKH());
        viewHolder.tvEmail.setText(khachHang.getEmail());
        viewHolder.tvSDT.setText(khachHang.getSdt());
        viewHolder.tvDiaChi.setText(khachHang.getDiaChi());
        Picasso.with(context).load(khachHang.getImageURL()).into(viewHolder.ivAnhDaiDien);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
