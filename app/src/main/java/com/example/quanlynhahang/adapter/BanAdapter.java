package com.example.quanlynhahang.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quanlynhahang.R;
import com.example.quanlynhahang.model.Ban;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BanAdapter extends RecyclerView.Adapter<BanAdapter.ViewHolder> {
    Context context;
    List<Ban> list;
    Ban ban;

    public BanAdapter(Context context,List<Ban> list) {
        this.context=context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvMaBan;
        public ImageView ivAnh;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivAnh=itemView.findViewById(R.id.imgAnh);
            tvMaBan=itemView.findViewById(R.id.tvMaBan);
        }

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Tao view va gan layout vao view
        View v = LayoutInflater.from(context)
                .inflate(R.layout.one_cell_ban, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        ban=list.get(i);

        viewHolder.tvMaBan.setText(ban.getmaBan());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

