package com.example.quanlynhahang.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.quanlynhahang.ChiTietGoiMonActivity;
import com.example.quanlynhahang.ChonMonAnActivity;
import com.example.quanlynhahang.GiaoDienNhanVienActivity;
import com.example.quanlynhahang.KhaiViFragment;
import com.example.quanlynhahang.MonDaChonFragment;
import com.example.quanlynhahang.QuanLyNguyenLieuActivity;
import com.example.quanlynhahang.R;
import com.example.quanlynhahang.dao.ChiTietBanDAO;
import com.example.quanlynhahang.model.Ban;
import com.example.quanlynhahang.model.ChiTietBan;
import com.example.quanlynhahang.model.MonAn;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MonAnAdapter2 extends BaseAdapter {
    List<MonAn> list;
    Context context;
    public LayoutInflater inflater;
    MonAnAdapter2.ViewHolder holder;

    public MonAnAdapter2(List<MonAn> list, Context context) {
        super();
        this.list = list;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final MonAn monAn = list.get(position);
        if (convertView == null) {
            holder = new MonAnAdapter2.ViewHolder();
            convertView = inflater.inflate(R.layout.one_cell_mon_an_2, null);

            //ánh xạ
            holder.ivAnhMonAn = (ImageView) convertView.findViewById(R.id.imgAnhMonAn);
            holder.ivAdd = (ImageView) convertView.findViewById(R.id.imgAdd);
            holder.tvTenMonAn = (TextView) convertView.findViewById(R.id.tvTenMonAn);

            //set data lên layout custom
            Picasso.with(context).load(monAn.getImageURL()).into(holder.ivAnhMonAn);
            holder.tvTenMonAn.setText(monAn.getTenMonAn());


            holder.ivAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context,monAn.getMaMonAn(),Toast.LENGTH_SHORT).show();

                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.chon_mon_layout);

                    final EditText edtSoLuong=dialog.findViewById(R.id.edtSoLuong);
                    final TextView tvTenMonAn=dialog.findViewById(R.id.tvTenMonAn);
                    final Button btnChon=dialog.findViewById(R.id.btnChon);

                    tvTenMonAn.setText(monAn.getTenMonAn());

                    btnChon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (TextUtils.isEmpty(edtSoLuong.getText().toString())) {
                                Toast.makeText(context, "mời nhập số lượng...", Toast.LENGTH_LONG).show();
                                return;
                            }


                            ChiTietBan chiTietBan=new ChiTietBan();
                            chiTietBan.setMaBan(((ChonMonAnActivity)context).layMaBan());
                            chiTietBan.setMonAn(monAn);
                            chiTietBan.setSoLuong(Integer.parseInt(edtSoLuong.getText().toString()));
                            ChiTietBanDAO chiTietBanDAO=new ChiTietBanDAO(context);
                            chiTietBanDAO.insert(chiTietBan);

                            dialog.dismiss();
                        }
                    });

                    dialog.show();

                }
            });

            convertView.setTag(holder);
        } else
            holder = (MonAnAdapter2.ViewHolder) convertView.getTag();
        return convertView;
    }

    public static class ViewHolder {
        ImageView ivAnhMonAn,ivAdd;
        TextView tvTenMonAn;
    }
}
