package com.example.quanlynhahang.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quanlynhahang.R;
import com.example.quanlynhahang.dao.MonAnDao;
import com.example.quanlynhahang.model.ChiTietBan;
import com.example.quanlynhahang.model.MonAn;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ChiTietBanAdapter extends RecyclerView.Adapter<ChiTietBanAdapter.ViewHolder> {
    Context context;
    List<ChiTietBan> list;
    ChiTietBan chiTietBan;

    public ChiTietBanAdapter(Context context,List<ChiTietBan> list) {
        this.context=context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvTenMonAn;
        public ImageView ivAnhMonAn;
        public EditText edtSoLuong;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivAnhMonAn=itemView.findViewById(R.id.imgAnhMonAn);
            tvTenMonAn=itemView.findViewById(R.id.tvTenMonAn);
            edtSoLuong=itemView.findViewById(R.id.edtSoLuong);
        }

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Tao view va gan layout vao view
        View v = LayoutInflater.from(context)
                .inflate(R.layout.one_cell_chon_mon, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        chiTietBan=list.get(i);

        viewHolder.tvTenMonAn.setText("Gỏi");
        viewHolder.edtSoLuong.setText("8");
        Picasso.with(context).load("a").into(viewHolder.ivAnhMonAn);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
