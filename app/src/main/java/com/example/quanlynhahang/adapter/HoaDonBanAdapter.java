package com.example.quanlynhahang.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quanlynhahang.R;
import com.example.quanlynhahang.model.HoaDonBan;

import java.util.List;

public class HoaDonBanAdapter extends RecyclerView.Adapter<HoaDonBanAdapter.ViewHolder> {
    Context context;
    List<HoaDonBan> list;
    HoaDonBan hoaDonBan;

    public HoaDonBanAdapter(Context context,List<HoaDonBan> list) {
        this.context=context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvMaHD,tvTenNhanVien,tvMaBan,tvThoiGian;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMaHD=itemView.findViewById(R.id.tvMaHD);
            tvTenNhanVien=itemView.findViewById(R.id.tvTenNhanVien);
            tvMaBan=itemView.findViewById(R.id.tvMaBan);
            tvThoiGian=itemView.findViewById(R.id.tvThoiGian);
        }

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Tao view va gan layout vao view
        View v = LayoutInflater.from(context)
                .inflate(R.layout.one_cell_hoa_don_ban, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        hoaDonBan=list.get(i);

        viewHolder.tvMaHD.setText(hoaDonBan.getMaHD());
        viewHolder.tvTenNhanVien.setText(hoaDonBan.getNhanVien().getTenNV());
        viewHolder.tvMaBan.setText(hoaDonBan.getMaBan());
        viewHolder.tvThoiGian.setText(hoaDonBan.getThoiGian());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
