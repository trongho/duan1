package com.example.quanlynhahang.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quanlynhahang.QuanLyMonAnActivity;
import com.example.quanlynhahang.R;
import com.example.quanlynhahang.model.MonAn;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MonAnAdapter extends RecyclerView.Adapter<MonAnAdapter.ViewHolder> {
    Context context;
    List<MonAn> list;
    MonAn monAn;

    public MonAnAdapter(Context context,List<MonAn> list) {
        this.context=context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tvMaLoai,tvTenMonAn,tvGiaTien;
        public ImageView ivAnhMonAn,ivDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivAnhMonAn=itemView.findViewById(R.id.imgAnhMonAn);
            tvMaLoai=itemView.findViewById(R.id.tvMaLoai);
            tvTenMonAn=itemView.findViewById(R.id.tvTenMonAn);
            tvGiaTien=itemView.findViewById(R.id.tvGiaTien);
            ivDelete=itemView.findViewById(R.id.ivDelete);

        }

    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Tao view va gan layout vao view
        View v = LayoutInflater.from(context)
                .inflate(R.layout.one_cell_mon_an, viewGroup, false);
        // gan cac thuoc tinh nhu size, margins, paddings.....
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        monAn=list.get(i);

        viewHolder.tvMaLoai.setText(monAn.getMaLoai());
        viewHolder.tvTenMonAn.setText(monAn.getTenMonAn());
        viewHolder.tvGiaTien.setText(monAn.getGiaTien()+"");
        Picasso.with(context).load(monAn.getImageURL()).into(viewHolder.ivAnhMonAn);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
