package com.example.quanlynhahang.dao;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.quanlynhahang.ChiTietGoiMonActivity;
import com.example.quanlynhahang.KhaiViFragment;
import com.example.quanlynhahang.MonChinhFragment;
import com.example.quanlynhahang.NuocUongFragment;
import com.example.quanlynhahang.QuanLyMonAnActivity;
import com.example.quanlynhahang.TrangMiengFragment;
import com.example.quanlynhahang.model.MonAn;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class MonAnDao {
    private DatabaseReference mDatabase;
    private Context context;
    String keyID;
    KhaiViFragment khaiViFragment;
    MonChinhFragment monChinhFragment;
    TrangMiengFragment trangMiengFragment;
    NuocUongFragment nuocUongFragment;

    public MonAnDao(Context context) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference("MonAn");
        this.context = context;
    }

    public MonAnDao(Context context,KhaiViFragment khaiViFragment) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference("MonAn");
        this.context = context;
        this.khaiViFragment=khaiViFragment;
    }

    public MonAnDao(Context context,MonChinhFragment monChinhFragment) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference("MonAn");
        this.context = context;
        this.monChinhFragment=monChinhFragment;
    }

    public MonAnDao(Context context,TrangMiengFragment trangMiengFragment) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference("MonAn");
        this.context = context;
        this.trangMiengFragment=trangMiengFragment;
    }

    public MonAnDao(Context context,NuocUongFragment nuocUongFragment) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference("MonAn");
        this.context = context;
        this.nuocUongFragment=nuocUongFragment;
    }


    public List<MonAn> getAll() {
        final List<MonAn> list = new ArrayList<MonAn>();
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    MonAn monAn = data.getValue(MonAn.class);
                    list.add(monAn);
                }
                ((QuanLyMonAnActivity)context).capNhatLV();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);
        return list;
    }

    public List<MonAn> getKhaiVi() {
        final List<MonAn> list = new ArrayList<MonAn>();
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if(data.child("maLoai").getValue(String.class).equals("KV")) {
                        MonAn monAn = data.getValue(MonAn.class);
                        list.add(monAn);
                    }
                    khaiViFragment.capNhatLV();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);
        return list;
    }

    public List<MonAn> getMonChinh() {
        final List<MonAn> list = new ArrayList<MonAn>();
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if(data.child("maLoai").getValue(String.class).equals("MC")) {
                        MonAn monAn = data.getValue(MonAn.class);
                        list.add(monAn);
                    }
                    monChinhFragment.capNhatLV();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);
        return list;
    }

    public List<MonAn> getTrangMieng() {
        final List<MonAn> list = new ArrayList<MonAn>();
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if(data.child("maLoai").getValue(String.class).equals("TM")) {
                        MonAn monAn = data.getValue(MonAn.class);
                        list.add(monAn);
                    }
                    trangMiengFragment.capNhatLV();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);
        return list;
    }

    public List<MonAn> getNuocUong() {
        final List<MonAn> list = new ArrayList<MonAn>();
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if(data.child("maLoai").getValue(String.class).equals("NU")) {
                        MonAn monAn = data.getValue(MonAn.class);
                        list.add(monAn);
                    }
                    nuocUongFragment.capNhatLV();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);
        return list;
    }


    public void insert(MonAn monAn) {
        keyID = mDatabase.push().getKey();
        mDatabase.child(keyID).setValue(monAn)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(context, "Thêm thành công", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(context, "Thêm thất bại", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    public void update(final MonAn monAn) {
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (data.child("maMonAn").getValue(String.class).equals(monAn.getMaMonAn())) {
                        keyID = data.getKey();
                        mDatabase.child(keyID).setValue(monAn)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(context, "Sửa thành công", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(context, "Sửa thất bại", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    public void delete(final MonAn monAn) {
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (data.child("maMonAn").getValue(String.class).equals(monAn.getMaMonAn())) {
                        keyID = data.getKey();
                        mDatabase.child(keyID).removeValue()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(context, "Xóa thành công", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(context, "Xóa thất bại", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
