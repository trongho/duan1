package com.example.quanlynhahang.dao;

import android.content.Context;
import android.util.Log;

import com.example.quanlynhahang.model.KhachHang;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class KhachHangDAO {
    private DatabaseReference mDatabase;
    private Context context;

    public KhachHangDAO(Context context) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference("KhachHang");
        this.context = context;
    }

    //lấy tất cả khách hàng
    public List<KhachHang> getAll() {
        final List<KhachHang> list = new ArrayList<KhachHang>();
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    KhachHang khachHang = data.getValue(KhachHang.class);
                    list.add(khachHang);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);
        return list;
    }
}
