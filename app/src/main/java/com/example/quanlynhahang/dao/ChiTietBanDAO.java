package com.example.quanlynhahang.dao;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.quanlynhahang.ChiTietGoiMonActivity;
import com.example.quanlynhahang.MonDaChonFragment;
import com.example.quanlynhahang.model.ChiTietBan;
import com.example.quanlynhahang.model.MonAn;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ChiTietBanDAO {
    private DatabaseReference mDatabase;
    private Context context;
    String keyID;
    MonDaChonFragment fragment;

    public ChiTietBanDAO(Context context) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference("ChiTietBan");
        this.context = context;
    }

    public ChiTietBanDAO(Context context,MonDaChonFragment fragment) {
        this.mDatabase = FirebaseDatabase.getInstance().getReference("ChiTietBan");
        this.context = context;
        this.fragment=fragment;
    }

    public List<ChiTietBan> getAll() {
        final List<ChiTietBan> list = new ArrayList<ChiTietBan>();
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    ChiTietBan chiTietBan = data.getValue(ChiTietBan.class);
                    list.add(chiTietBan);
                }
                fragment.capNhatLV();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);
        return list;
    }

    public List<ChiTietBan> getByMaBan(final String maBan) {
        final List<ChiTietBan> list = new ArrayList<ChiTietBan>();
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if(data.child("maBan").getValue(String.class).equals(maBan)) {
                        ChiTietBan chiTietBan = data.getValue(ChiTietBan.class);
                        list.add(chiTietBan);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
            }
        };
        mDatabase.addValueEventListener(postListener);
        return list;
    }

    public void insert(ChiTietBan chiTietBan) {
        keyID = mDatabase.push().getKey();
        chiTietBan.setMaChiTietBan(mDatabase.push().getKey());
        mDatabase.child(keyID).setValue(chiTietBan)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(context, "Thêm thành công", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(context, "Thêm thất bại", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void delete(final ChiTietBan chiTietBan) {
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    if (data.child("maChiTietBan").getValue(String.class).equals(chiTietBan.getMaChiTietBan())){
                        keyID = data.getKey();
                        mDatabase.child(keyID).removeValue()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(context, "Xóa thành công", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(context, "Xóa thất bại", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
