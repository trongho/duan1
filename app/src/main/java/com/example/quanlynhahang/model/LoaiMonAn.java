package com.example.quanlynhahang.model;

import androidx.annotation.NonNull;

public class LoaiMonAn {
    String maLoai,tenLoai;

    public LoaiMonAn(String maLoai, String tenLoai) {
        this.maLoai = maLoai;
        this.tenLoai = tenLoai;
    }

    public LoaiMonAn() {
    }

    public String getMaLoai() {
        return maLoai;
    }

    public void setMaLoai(String maLoai) {
        this.maLoai = maLoai;
    }

    public String getTenLoai() {
        return tenLoai;
    }

    public void setTenLoai(String tenLoai) {
        this.tenLoai = tenLoai;
    }

    @NonNull
    @Override
    public String toString() {
        return maLoai+"-"+tenLoai;
    }
}
