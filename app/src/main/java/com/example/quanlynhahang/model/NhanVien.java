package com.example.quanlynhahang.model;

public class NhanVien {
    private String tenNV;
    private String email;
    private String password;

    public NhanVien(String tenNV, String email, String password) {
        this.tenNV = tenNV;
        this.email= email;
        this.password = password;
    }

    public NhanVien() {
    }

    public String getTenNV() {
        return tenNV;
    }

    public void setTenNV(String tenNV) {
        this.tenNV = tenNV;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
