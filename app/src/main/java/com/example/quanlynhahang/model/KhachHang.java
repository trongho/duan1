package com.example.quanlynhahang.model;

public class KhachHang {
    private String tenKH;
    private String password;
    private String email;
    private String sdt;
    private String diaChi;
    private String imageURL;

    public KhachHang(String tenKH,String password, String email, String sdt, String diaChi,String imageURL) {
        this.tenKH = tenKH;
        this.password = password;
        this.email = email;
        this.sdt = sdt;
        this.diaChi = diaChi;
        this.imageURL=imageURL;
    }

    public KhachHang() {
    }

    public String getTenKH() {
        return tenKH;
    }

    public void setTenKH(String tenKH) {
        this.tenKH = tenKH;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
