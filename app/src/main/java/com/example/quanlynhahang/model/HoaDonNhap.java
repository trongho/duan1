package com.example.quanlynhahang.model;

public class HoaDonNhap {
    String maHD;
    String thoiGian;

    public HoaDonNhap(String maHD, String thoiGian) {
        this.maHD = maHD;
        this.thoiGian = thoiGian;
    }

    public HoaDonNhap() {
    }

    public String getMaHD() {
        return maHD;
    }

    public void setMaHD(String maHD) {
        this.maHD = maHD;
    }

    public String getThoiGian() {
        return thoiGian;
    }

    public void setThoiGian(String thoiGian) {
        this.thoiGian = thoiGian;
    }
}
