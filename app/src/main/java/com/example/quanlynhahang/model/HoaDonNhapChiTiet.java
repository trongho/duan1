package com.example.quanlynhahang.model;

public class HoaDonNhapChiTiet {
    String maHD,maNguyenLieu,maNhanVien;
    int soLuong,giaTien;

    public HoaDonNhapChiTiet(String maHD, String maNguyenLieu, String maNhanVien, int soLuong, int giaTien) {
        this.maHD = maHD;
        this.maNguyenLieu = maNguyenLieu;
        this.maNhanVien = maNhanVien;
        this.soLuong = soLuong;
        this.giaTien = giaTien;
    }

    public HoaDonNhapChiTiet() {
    }

    public String getMaHD() {
        return maHD;
    }

    public void setMaHD(String maHD) {
        this.maHD = maHD;
    }

    public String getMaNguyenLieu() {
        return maNguyenLieu;
    }

    public void setMaNguyenLieu(String maNguyenLieu) {
        this.maNguyenLieu = maNguyenLieu;
    }

    public String getMaNhanVien() {
        return maNhanVien;
    }

    public void setMaNhanVien(String maNhanVien) {
        this.maNhanVien = maNhanVien;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public int getGiaTien() {
        return giaTien;
    }

    public void setGiaTien(int giaTien) {
        this.giaTien = giaTien;
    }
}
