package com.example.quanlynhahang.model;

public class Ban {
    String maBan,viTri,trangThai;

    public Ban(String maBan, String viTri, String trangThai) {
        this.maBan = maBan;
        this.viTri = viTri;
        this.trangThai = trangThai;
    }

    public Ban() {
    }

    public String getmaBan() {
        return maBan;
    }

    public void setmaBan(String maBan) {
        this.maBan = maBan;
    }

    public String getViTri() {
        return viTri;
    }

    public void setViTri(String viTri) {
        this.viTri = viTri;
    }

    public String getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(String trangThai) {
        this.trangThai = trangThai;
    }
}
