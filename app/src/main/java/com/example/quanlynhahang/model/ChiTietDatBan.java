package com.example.quanlynhahang.model;

public class ChiTietDatBan {
    String maKH,maBan,thoiGian;

    public ChiTietDatBan(String maKH, String maBan, String thoiGian) {
        this.maKH = maKH;
        this.maBan = maBan;
        this.thoiGian = thoiGian;
    }

    public ChiTietDatBan() {
    }

    public String getMaKH() {
        return maKH;
    }

    public void setMaKH(String maKH) {
        this.maKH = maKH;
    }

    public String getMaBan() {
        return maBan;
    }

    public void setMaBan(String maBan) {
        this.maBan = maBan;
    }

    public String getThoiGian() {
        return thoiGian;
    }

    public void setThoiGian(String thoiGian) {
        this.thoiGian = thoiGian;
    }
}
