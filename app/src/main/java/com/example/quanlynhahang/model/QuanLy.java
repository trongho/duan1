package com.example.quanlynhahang.model;

public class QuanLy {
    private String tenQL;
    private String email;
    private String password;

    public QuanLy(String tenQL, String email, String password) {
        this.tenQL = tenQL;
        this.email = email;
        this.password = password;
    }

    public QuanLy() {
    }

    public String getTenQL() {
        return tenQL;
    }

    public void setTenQL(String tenQL) {
        this.tenQL = tenQL;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
