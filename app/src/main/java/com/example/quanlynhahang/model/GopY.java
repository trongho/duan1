package com.example.quanlynhahang.model;

import java.util.Date;

public class GopY {
    private String maGopY,noiDung;
    private String thoiGian;
    private String email;

    public GopY(String maGopY, String noiDung, String thoiGian,String email) {
        this.maGopY = maGopY;
        this.noiDung = noiDung;
        this.thoiGian = thoiGian;
        this.email=email;
    }

    public GopY() {
    }

    public String getMaGopY() {
        return maGopY;
    }

    public void setMaGopY(String maGopY) {
        this.maGopY = maGopY;
    }

    public String getNoiDung() {
        return noiDung;
    }

    public void setNoiDung(String noiDung) {
        this.noiDung = noiDung;
    }

    public String getThoiGian() {
        return thoiGian;
    }

    public void setThoiGian(String thoiGian) {
        this.thoiGian = thoiGian;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
