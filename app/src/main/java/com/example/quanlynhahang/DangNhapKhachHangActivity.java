package com.example.quanlynhahang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class DangNhapKhachHangActivity extends AppCompatActivity {
    EditText edtEmail,edtPassword;
    CheckBox chkRemember;
    Button btnLogin,btnRegister,btnForgotPassword;
    FirebaseAuth mAuth;

    public static String  PREFS_NAME="mypre";
    public static String PREF_EMAIL="email";
    public static String PREF_PASSWORD="password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dang_nhap_khach_hang);

        edtEmail=findViewById(R.id.edtEmail);
        edtPassword=findViewById(R.id.edtPassword);
        chkRemember=findViewById(R.id.chkNhoMatKhau);
        btnLogin=findViewById(R.id.btnLogin);
        btnRegister=findViewById(R.id.btnRegister);
        btnForgotPassword=findViewById(R.id.btn_forgot_password);

        mAuth=FirebaseAuth.getInstance();

        login();
        register();
        forgotPassword();
    }

    public void login(){
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email=edtEmail.getText().toString();
                final String password=edtPassword.getText().toString();

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Please enter email...", Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Please enter password!", Toast.LENGTH_LONG).show();
                    return;
                }

                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {

                                    if(chkRemember.isChecked())
                                        rememberMe(email,password); //save username and password

                                    Toast.makeText(getApplicationContext(), "Dang nhap thanh cong", Toast.LENGTH_LONG).show();

                                    Intent intent = new Intent(DangNhapKhachHangActivity.this, GiaoDienKhachHangActivity.class);
                                    startActivity(intent);
                                }
                                else {
                                    Toast.makeText(getApplicationContext(), "Dang nhap that bai", Toast.LENGTH_LONG).show();

                                }
                            }
                        });
            }
        });

    }

    public void rememberMe(String email, String password){
        //save username and password in SharedPreferences
        getSharedPreferences(PREFS_NAME,MODE_PRIVATE)
                .edit()
                .putString(PREF_EMAIL,email)
                .putString(PREF_PASSWORD,password)
                .commit();
    }

    public void register(){
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DangNhapKhachHangActivity.this,DangKyKhachHangActivity.class);
                startActivity(intent);
            }
        });
    }

    public void forgotPassword(){
        btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DangNhapKhachHangActivity.this,QuenMatKhauActivity.class);
                startActivity(intent);
            }
        });
    }
}
