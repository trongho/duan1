package com.example.quanlynhahang;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.quanlynhahang.adapter.ChiTietBanAdapter;
import com.example.quanlynhahang.adapter.MonAnAdapter;
import com.example.quanlynhahang.adapter.MonAnAdapter2;
import com.example.quanlynhahang.dao.ChiTietBanDAO;
import com.example.quanlynhahang.dao.MonAnDao;
import com.example.quanlynhahang.model.ChiTietBan;
import com.example.quanlynhahang.model.MonAn;

import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Templates;

public class ChiTietGoiMonActivity extends AppCompatActivity {
    RecyclerView rv_dsma;
    Context c;

    List<ChiTietBan> list;
    ChiTietBanDAO chiTietBanDAO;
    ChiTietBanAdapter chiTietBanAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chi_tiet_goi_mon);

        rv_dsma=findViewById(R.id.rv_dsma);
        rv_dsma.setHasFixedSize(true);
        rv_dsma.setLayoutManager(new LinearLayoutManager(c));

        showCTB();
    }

    public void showCTB(){
        chiTietBanDAO=new ChiTietBanDAO(ChiTietGoiMonActivity.this);
        list=new ArrayList<ChiTietBan>();
        list=chiTietBanDAO.getByMaBan("T101");
        chiTietBanAdapter=new ChiTietBanAdapter(ChiTietGoiMonActivity.this,list);
        rv_dsma.setAdapter(chiTietBanAdapter);
    }

    public void capNhatLV(){
        chiTietBanAdapter.notifyItemInserted(list.size());
        chiTietBanAdapter.notifyDataSetChanged();
    }
}
