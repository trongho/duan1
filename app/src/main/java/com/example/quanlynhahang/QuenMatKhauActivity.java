package com.example.quanlynhahang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class QuenMatKhauActivity extends AppCompatActivity {
    EditText edtEmail;
    Button btnLayLaiMk;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quen_mat_khau);

        edtEmail=findViewById(R.id.edtEmail);
        btnLayLaiMk=findViewById(R.id.btn_lay_lai_mk);

        mAuth = FirebaseAuth.getInstance();
        btnLayLaiMk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.sendPasswordResetEmail(edtEmail.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            //Neu dung se co mail toi bao nhap Password moi
                            Toast.makeText(QuenMatKhauActivity.this, "Kiểm tra mail của bạn", Toast.LENGTH_SHORT).show();
                        } else {
                            //Sai se thong bao loi
                            Toast.makeText(QuenMatKhauActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        });

    }
}
