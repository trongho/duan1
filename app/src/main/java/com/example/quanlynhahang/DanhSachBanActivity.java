package com.example.quanlynhahang;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.quanlynhahang.adapter.BanAdapter;
import com.example.quanlynhahang.adapter.BanAdapter2;
import com.example.quanlynhahang.dao.BanDAO;
import com.example.quanlynhahang.model.Ban;

import java.util.ArrayList;
import java.util.List;

public class DanhSachBanActivity extends AppCompatActivity {
    GridView gv1;

    List<Ban> list;
    BanDAO banDAO;
    BanAdapter2 banAdapter2;
    Ban ban;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_danh_sach_ban);

        gv1=findViewById(R.id.gv1);

        showBan();
    }

    public void showBan(){
        banDAO=new BanDAO(DanhSachBanActivity.this);
        list=new ArrayList<Ban>();
        list=banDAO.getAll2();
        banAdapter2=new BanAdapter2(list,DanhSachBanActivity.this);
        gv1.setAdapter(banAdapter2);

    }

    public void capNhatLV(){
        banAdapter2.notifyDataSetChanged();
    }
}
