package com.example.quanlynhahang;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class DangNhapQLNVActivity extends AppCompatActivity {
    Button btnLogin;
    EditText edtUsername,edtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dang_nhap_qlnv);

        initComponent();
        xuLyDangNhap();
    }

    public void initComponent(){
        btnLogin=findViewById(R.id.btnLogin);
        edtUsername=findViewById(R.id.edtUsername);
        edtPassword=findViewById(R.id.edtPassword);
    }

    public void xuLyDangNhap(){
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username=edtUsername.getText().toString();
                String password=edtPassword.getText().toString();
                if(username.equals("admin")&&password.equals("admin")){
                    Intent intent=new Intent(DangNhapQLNVActivity.this,GiaoDienQuanLyActivity.class);
                    startActivity(intent);
                }
                else if(username.equals("staff")&&password.equals("staff")){
                    Intent intent=new Intent(DangNhapQLNVActivity.this,GiaoDienNhanVienActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}
