package com.example.quanlynhahang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.quanlynhahang.adapter.KhachHangAdapter;
import com.example.quanlynhahang.model.KhachHang;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class QuanLyKhachHangActivity extends AppCompatActivity {
    RecyclerView rv_dskh;
    Context c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quan_ly_khach_hang);

        rv_dskh=findViewById(R.id.rv_dskh);
        rv_dskh.setHasFixedSize(true);
        rv_dskh.setLayoutManager(new LinearLayoutManager(c));
    }

    @Override
    protected void onStart() {
        super.onStart();

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("KhachHang");

        FirebaseRecyclerOptions<KhachHang> options =
                new FirebaseRecyclerOptions.Builder<KhachHang>()
                        .setQuery(databaseReference,KhachHang.class)
                        .build();

        FirebaseRecyclerAdapter<KhachHang, KhachHangAdapter.ViewHolder> adapter =
                new FirebaseRecyclerAdapter<KhachHang, KhachHangAdapter.ViewHolder>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull final KhachHangAdapter.ViewHolder holder, int position, @NonNull KhachHang model) {
                        final String key = getRef(position).getKey();
                        databaseReference.child(key).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.hasChild("imageURL")){
                                    String tenKH = dataSnapshot.child("tenKH").getValue().toString();
                                    String email = dataSnapshot.child("email").getValue().toString();
                                    String sdt = dataSnapshot.child("sdt").getValue().toString();
                                    String diaChi = dataSnapshot.child("diaChi").getValue().toString();
                                    String imageURL = dataSnapshot.child("imageURL").getValue().toString();

                                    holder.tvTenKH.setText(tenKH);
                                    holder.tvEmail.setText(email);
                                    holder.tvSDT.setText(sdt);
                                    holder.tvDiaChi.setText(diaChi);
                                    Picasso.with(QuanLyKhachHangActivity.this).load(imageURL).into(holder.ivAnhDaiDien);
                                }

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                    }

                    @NonNull
                    @Override
                    public KhachHangAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.one_cell_khach_hang,parent,false);
                        KhachHangAdapter.ViewHolder viewHolder = new KhachHangAdapter.ViewHolder(view);
                        return viewHolder;
                    }
                };

        rv_dskh.setAdapter(adapter);
        adapter.startListening();
    }
}
